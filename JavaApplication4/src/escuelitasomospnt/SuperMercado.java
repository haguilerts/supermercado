/** * @author Giovanny Aguilar Rojas */
package escuelitasomospnt;

import java.util.*;

public class SuperMercado {
    /*
    * PRE CONDICION: ingresara una lista vacia donde se le agregara obejtos con sus atributos inicializadodos(Productos).
    * POST CONDICION: retornara una lista de productos   
    */
    public static ArrayList<Productos> crear_productos( ArrayList<Productos> lista) {                      
        Bebidas gaseosa=new Bebidas("Coca-Cola Zero", (float) 1.5, 20);
        Bebidas gaseosa2=new Bebidas("Coca-Cola", (float) 1.5, 18);
        Shampoo shampoo=new Shampoo("Shampoo sedal",500,19);
        Frutas fruta=new Frutas("Frutillas", 64, "kilo");        
        lista.add(gaseosa);
        lista.add(gaseosa2);
        lista.add(shampoo);
        lista.add(fruta);  
        return lista;   
    }
    /*
    * PRE CONDICION: ingresara la lista de productos.
    * POST CONDICION: se obtiene los productos con el precio maximo y minimo.
    */
    public static void mostra_producto(ArrayList<Productos> lista) {
        int maxPrecio=0; 
        int minPrecio=0;
        String nombreMax="";
        String nombreMin="";
        for(int i=0; i <lista.size(); i++){
                if(lista.get(i).getPrecio()==lista.get(0).getPrecio()){
                    maxPrecio=lista.get(0).getPrecio();
                    minPrecio=lista.get(0).getPrecio();                    
                } else if(maxPrecio < lista.get(i).getPrecio() ){
                   maxPrecio=lista.get(i).getPrecio();   
                   nombreMax=lista.get(i).getNombre();
                }
                if( minPrecio >= lista.get(i).getPrecio() ) {
                    minPrecio=lista.get(i).getPrecio();
                    nombreMin=lista.get(i).getNombre();                    
                }                  
        }   
        System.out.println("Producto más caro: "+nombreMax );
        System.out.println("Producto más barato: "+nombreMin);
    }

    public static void main(String[] args) {
        ArrayList<Productos> listaProducto = new ArrayList(); 
        crear_productos(listaProducto);
        mostra_producto(listaProducto);        
    }        
}




/*
Imaginemos un supermercado de barrio.
Crear un programa cuyo punto de entrada sea un main en donde,
al ejecutarse, se visualiza lo siguiente por consola y se termina la ejecución:
    Nombre: Coca-Cola Zero /// Litros: 1.5 /// Precio: $20
    Nombre: Coca-Cola /// Litros: 1.5 /// Precio: $18
    Nombre: Shampoo Sedal /// Contenido: 500ml /// Precio: $19
    Nombre: Frutillas /// Precio: $64 /// Unidad de venta: kilo
=============================
Producto más caro: Frutillas
Producto más barato: Coca-Cola

La solución debe cumplir con los siguientes requisitos:
    1) Diseñar una solución orientada a objetos.
    2) La salida es por consola y exactamente como se requiere.
    3) Usar solamente las clases provistas por Java 8..
    4) Cargar la lista de productos en un único método. No hay ingreso por pantalla de ningún tipo.
    5) El algoritmo usado para la impresión no tiene que depender de la cantidad o tipo de productos.
    6) Para mostrar el mayor / menor, utilizar la interfaz Comparable.
    7) Para imprimir por pantalla, sobrescribir el método toString()

https://somospnt.com/candidatos
https://docs.google.com/document/u/1/d/19eAluIoBt9l3UQJ8jmFy0UHuFOtc0GjP50AX3wgqTvM/pub
*/
