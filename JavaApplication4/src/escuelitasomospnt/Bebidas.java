
package escuelitasomospnt;
//import escuelitasomospnt.Productos;
public class Bebidas extends Productos{
    private float litros;

    public Bebidas(String nombre, float litros,  int precio) {
        super(nombre, precio);
        this.litros = litros;
    }

    public float getLitros() {
        return litros;
    }

    public void setLitros(float litros) {
        this.litros = litros;
    }
    

    @Override
    public String toString() {
        return "Nombre: "+getNombre()+ "    Litros: " + litros +"ml"+"      Precio: $"+ getPrecio()+ '}';
    }
    
}
