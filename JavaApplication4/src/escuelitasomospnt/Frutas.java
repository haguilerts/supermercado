
package escuelitasomospnt;

public class Frutas extends Productos{
    private String unidadDeVenta;

    public Frutas(String nombre, int precio,String unidadDeVenta) {
        super(nombre, precio);
        this.unidadDeVenta = unidadDeVenta;
    }

    public String getUnidadDeVenta() {
        return unidadDeVenta;
    }

    public void setUnidadDeVenta(String unidadDeVenta) {
        this.unidadDeVenta = unidadDeVenta;
    }

    @Override
    public String toString() {
        return "Nombre: "+getNombre()+"     Precio: $"+getPrecio()+ "    Unidad de Venta: " + unidadDeVenta ;
    }
    
}
